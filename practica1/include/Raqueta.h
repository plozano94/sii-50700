// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#if !defined(AFX_RAQUETA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_RAQUETA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve();
};
#endif