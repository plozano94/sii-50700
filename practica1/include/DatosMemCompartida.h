#pragma once 

#include "Esfera.h"
#include "Raqueta.h"
#if !defined(AFX_DatosMemCompartida_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_DatosMemCompartida_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_
class DatosMemCompartida
{       
public:         
      Esfera esfera;
      Raqueta raqueta1;
      int accion; //1 arriba, 0 nada, -1 abajo
};
#endif