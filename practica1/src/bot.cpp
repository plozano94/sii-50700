#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"

int main(){
	int file;


	DatosMemCompartida* pDmc;
	char* org;

	file=open("/tmp/datosComp.txt",O_RDWR);
	
	org=(char*)mmap(NULL,sizeof(*(pDmc)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	close(file);
	
	pDmc=(DatosMemCompartida*)org;

	float pos;
	while(1){
		
		pos=(pDmc->raqueta1.y2+pDmc->raqueta1.y1)/2;
		if(pos<pDmc->esfera.centro.y)
			pDmc->accion=1;
		else if(pos>pDmc->esfera.centro.y)
			pDmc->accion=-1;
		else
			pDmc->accion=0;
		usleep(25000);
	}
	munmap(org,sizeof(*(pDmc)));


}
